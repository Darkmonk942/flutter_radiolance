
import 'package:all_vet_pharm/model/AuthModel.dart';
import 'package:all_vet_pharm/model/EventModel.dart';
import 'package:all_vet_pharm/model/RequestFullModel.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
part 'RestClient.g.dart';

@RestApi(baseUrl: "https://api.stage.radiolance.dev.sdh.com.ua/v1/")
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  @POST("auth/token/")
  @FormUrlEncoded()
  Future<AuthModel> userLogin(
      @Field("email") String email,
      @Field("password") String pass);

  @POST("notifications/push/gcm/")
  @FormUrlEncoded()
  Future<void> sendFCMToken(
      @Field("cloud_message_type") String type,
      @Field("registration_id") String token,
      @Field("active") bool enable);

  @POST("notifications/push/apns/")
  @FormUrlEncoded()
  Future<void> sendAPNSToken(
      @Field("registration_id") String token,
      @Field("active") bool enable);

  @GET("service-requests/")
  Future<EventModel> getRequests();

  @GET("service-requests/{id}")
  Future<RequestFullModel> getRequest(
      @Path("id") int id);
}