import 'package:flutter/material.dart';

abstract class AppStyles {
  static const TextStyle textMain = TextStyle(
      color: Colors.white,
      fontSize: 16,
      fontFamily: 'RobotoRegular'
  );

  static const TextStyle hintMain = TextStyle(
    color: colorTextHint,
    fontSize: 14,
    fontFamily: 'RobotoRegular'
  );

  static const TextStyle textSecondary = TextStyle(
      color: colorTextSecondary,
      fontSize: 14,
      fontFamily: 'RobotoRegular'
  );

  static const Color colorTextHint = Color(0xFFA1A4A9);
  static const Color colorTextSecondary = Color(0xFFA1A4A9);
}