import 'package:flutter/material.dart';

class AppColors {
  final primary = const Color(0xFF222736);
  final secondary = const Color(0xFF2F3548);
  final accent = const Color(0xFF0CB3BD);

  final textHint = const Color(0xFFA1A4A9);

  final editBorder = const Color(0xFF6E7379);
  final editBorderEnable = const Color(0xFFB1ECEF);

  final dividerMain = const Color(0xFF484E5F);

  final iconTint = const Color(0xFFB1ECEF);

  const AppColors();
}