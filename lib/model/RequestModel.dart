import 'package:all_vet_pharm/model/ZoneModel.dart';
import 'package:json_annotation/json_annotation.dart';
import 'CustomerModel.dart';
part 'RequestModel.g.dart';

@JsonSerializable()
class RequestModel {

  @JsonKey(name: "id")
  final int id;

  @JsonKey(name: "number")
  final String number;

  @JsonKey(name: "modality")
  final String modality;

  @JsonKey(name: "publish_date")
  final String date;

  @JsonKey(name: "has_been_viewed")
  final bool isViewed;

  @JsonKey(name: "customer")
  final CustomerModel customer;

  @JsonKey(name: "zones")
  final List<ZoneModel> zones;

  RequestModel(this.id, this.number, this.modality, this.date, this.isViewed,
      this.customer, this.zones);

  factory RequestModel.fromJson(Map<String, dynamic> json) => _$RequestModelFromJson(json);

  Map<String, dynamic> toJson() => _$RequestModelToJson(this);
}