// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'RequestFullModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RequestFullModel _$RequestFullModelFromJson(Map<String, dynamic> json) {
  return RequestFullModel(
    json['id'] as int,
    json['number'] as String,
    json['modality'] as String,
    json['clinical_task'] as String,
    json['complains'] as String,
    json['equipment'] as String,
    json['publish_date'] as String,
    CustomerModel.fromJson(json['customer'] as Map<String, dynamic>),
    PatientModel.fromJson(json['patient'] as Map<String, dynamic>),
    MotivationModel.fromJson(json['motivation'] as Map<String, dynamic>),
    (json['services'] as List<dynamic>)
        .map((e) => ServiceModel.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$RequestFullModelToJson(RequestFullModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'number': instance.number,
      'modality': instance.modality,
      'clinical_task': instance.clinicalTask,
      'complains': instance.complains,
      'equipment': instance.equipment,
      'publish_date': instance.date,
      'customer': instance.customer,
      'patient': instance.patient,
      'motivation': instance.motivation,
      'services': instance.services,
    };
