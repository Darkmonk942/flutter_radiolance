import 'package:json_annotation/json_annotation.dart';
part 'PatientModel.g.dart';

@JsonSerializable()
class PatientModel {

  @JsonKey(name: "gender")
  final String gender;

  @JsonKey(name: "age")
  final int age;

  bool isMale() {
    print(gender);
    if(gender == "male") {
      return true;
    }

    return false;
  }

  PatientModel(this.gender, this.age);

  factory PatientModel.fromJson(Map<String, dynamic> json) => _$PatientModelFromJson(json);

  Map<String, dynamic> toJson() => _$PatientModelToJson(this);
}