import 'package:json_annotation/json_annotation.dart';
part 'ServiceModel.g.dart';

@JsonSerializable()
class ServiceModel {

  @JsonKey(name: "name")
  final String name;

  ServiceModel(this.name);

  factory ServiceModel.fromJson(Map<String, dynamic> json) => _$ServiceModelFromJson(json);

  Map<String, dynamic> toJson() => _$ServiceModelToJson(this);
}