import 'package:json_annotation/json_annotation.dart';
part 'CustomerModel.g.dart';

@JsonSerializable()
class CustomerModel {

  @JsonKey(name: "name")
  final String name;

  @JsonKey(name: "logotype")
  final String? logoUrl;

  CustomerModel(this.name, this.logoUrl);

  factory CustomerModel.fromJson(Map<String, dynamic> json) => _$CustomerModelFromJson(json);

  Map<String, dynamic> toJson() => _$CustomerModelToJson(this);
}