import 'package:json_annotation/json_annotation.dart';
part 'ZoneModel.g.dart';

@JsonSerializable()
class ZoneModel {

  @JsonKey(name: "name")
  final String name;

  ZoneModel(this.name);

  factory ZoneModel.fromJson(Map<String, dynamic> json) => _$ZoneModelFromJson(json);

  Map<String, dynamic> toJson() => _$ZoneModelToJson(this);
}