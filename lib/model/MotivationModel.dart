import 'package:json_annotation/json_annotation.dart';
part 'MotivationModel.g.dart';

@JsonSerializable()
class MotivationModel {

  @JsonKey(name: "amount")
  final double amount;

  @JsonKey(name: "currency")
  final String currency;

  MotivationModel(this.amount, this.currency);

  factory MotivationModel.fromJson(Map<String, dynamic> json) => _$MotivationModelFromJson(json);

  Map<String, dynamic> toJson() => _$MotivationModelToJson(this);
}