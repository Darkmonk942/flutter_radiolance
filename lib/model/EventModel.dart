import 'package:all_vet_pharm/model/RequestModel.dart';
import 'package:json_annotation/json_annotation.dart';
part 'EventModel.g.dart';

@JsonSerializable()
class EventModel {

  @JsonKey(name: "results")
  final List<RequestModel>? requests;

  EventModel({this.requests});

  factory EventModel.fromJson(Map<String, dynamic> json) => _$EventModelFromJson(json);

  Map<String, dynamic> toJson() => _$EventModelToJson(this);
}