// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CustomerModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CustomerModel _$CustomerModelFromJson(Map<String, dynamic> json) {
  return CustomerModel(
    json['name'] as String,
    json['logotype'] as String?,
  );
}

Map<String, dynamic> _$CustomerModelToJson(CustomerModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'logotype': instance.logoUrl,
    };
