// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'RequestModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RequestModel _$RequestModelFromJson(Map<String, dynamic> json) {
  return RequestModel(
    json['id'] as int,
    json['number'] as String,
    json['modality'] as String,
    json['publish_date'] as String,
    json['has_been_viewed'] as bool,
    CustomerModel.fromJson(json['customer'] as Map<String, dynamic>),
    (json['zones'] as List<dynamic>)
        .map((e) => ZoneModel.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$RequestModelToJson(RequestModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'number': instance.number,
      'modality': instance.modality,
      'publish_date': instance.date,
      'has_been_viewed': instance.isViewed,
      'customer': instance.customer,
      'zones': instance.zones,
    };
