// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MotivationModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MotivationModel _$MotivationModelFromJson(Map<String, dynamic> json) {
  return MotivationModel(
    (json['amount'] as num).toDouble(),
    json['currency'] as String,
  );
}

Map<String, dynamic> _$MotivationModelToJson(MotivationModel instance) =>
    <String, dynamic>{
      'amount': instance.amount,
      'currency': instance.currency,
    };
