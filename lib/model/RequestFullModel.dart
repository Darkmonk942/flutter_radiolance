import 'package:all_vet_pharm/model/CustomerModel.dart';
import 'package:all_vet_pharm/model/MotivationModel.dart';
import 'package:all_vet_pharm/model/PatientModel.dart';
import 'package:all_vet_pharm/model/ServiceModel.dart';
import 'package:json_annotation/json_annotation.dart';
part 'RequestFullModel.g.dart';

@JsonSerializable()
class RequestFullModel {

  @JsonKey(name: "id")
  final int id;

  @JsonKey(name: "number")
  final String number;

  @JsonKey(name: "modality")
  final String modality;

  @JsonKey(name: "clinical_task")
  final String clinicalTask;

  @JsonKey(name: "complains")
  final String complains;

  @JsonKey(name: "equipment")
  final String equipment;

  @JsonKey(name: "publish_date")
  final String date;

  @JsonKey(name: "customer")
  final CustomerModel customer;

  @JsonKey(name: "patient")
  final PatientModel patient;

  @JsonKey(name: "motivation")
  final MotivationModel motivation;

  @JsonKey(name: "services")
  final List<ServiceModel> services;

  String getAmount() {
    return motivation.amount.toString() + " " + motivation.currency;
  }

  RequestFullModel(this.id, this.number, this.modality, this.clinicalTask, this.complains,
      this.equipment, this.date, this.customer, this.patient, this.motivation, this.services);

  factory RequestFullModel.fromJson(Map<String, dynamic> json) => _$RequestFullModelFromJson(json);

  Map<String, dynamic> toJson() => _$RequestFullModelToJson(this);

}