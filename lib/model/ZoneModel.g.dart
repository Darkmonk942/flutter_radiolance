// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ZoneModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ZoneModel _$ZoneModelFromJson(Map<String, dynamic> json) {
  return ZoneModel(
    json['name'] as String,
  );
}

Map<String, dynamic> _$ZoneModelToJson(ZoneModel instance) => <String, dynamic>{
      'name': instance.name,
    };
