// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'PatientModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PatientModel _$PatientModelFromJson(Map<String, dynamic> json) {
  return PatientModel(
    json['gender'] as String,
    json['age'] as int,
  );
}

Map<String, dynamic> _$PatientModelToJson(PatientModel instance) =>
    <String, dynamic>{
      'gender': instance.gender,
      'age': instance.age,
    };
