import 'dart:io';

import 'package:all_vet_pharm/navigation.dart';
import 'package:all_vet_pharm/network/RestClient.dart';
import 'package:all_vet_pharm/second_screen.dart';
import 'package:all_vet_pharm/theme/colors.dart';
import 'package:dio/dio.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
// import 'package:firebase_core/firebase_core.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:device_preview/device_preview.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

void main()  {
  runApp(CheckPlatform().getWidget());
}

class CheckPlatform {
  Widget getWidget() {
    if (Platform.isIOS) {
      return AndroidUI();
    } else {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          systemNavigationBarColor: AppColors().primary));
      return AndroidUI();
    }
  }
}

class AndroidUI extends StatefulWidget {
  @override
  _AndroidUIState createState() => _AndroidUIState();
}

class IosUi extends StatefulWidget {
  @override
  _IosUIState createState() => _IosUIState();
}

class _AndroidUIState extends State<AndroidUI> {

  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  bool _isObscure = true;
  bool _emailValidation = true;
  bool _passValidation = true;

  void initializeFlutterFire() async {
    await Firebase.initializeApp();
    await Hive.initFlutter();
    var tokenBox = await Hive.openBox('main');
    if(tokenBox.get('authToken', defaultValue: null ) != null) {
      NavigationService.instance.navigateToReplacement("second");
    }
    FirebaseMessaging messaging = FirebaseMessaging.instance;

    NotificationSettings settings = await messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    if(Platform.isAndroid) {
      // Get Firebase token for push notifications
      messaging.getToken().then((value) => saveFCMToken(value));
    }

    // Get APNS token for push notifications
    if(settings.authorizationStatus == AuthorizationStatus.authorized && Platform.isIOS) {
      messaging.getAPNSToken().then((value) => saveAPNSToken(value));
    }

  }

  @override
  void initState() {
    initializeFlutterFire();
    super.initState();
  }

  @override
  Widget build(BuildContext buildContext) {
    return MaterialApp(
      localizationsDelegates: const [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate
      ],
      supportedLocales: const [
        Locale('en', ''),
        Locale('ru', '')
      ],
      navigatorKey: NavigationService.instance.navigationKey,
      initialRoute: "login",
      routes: {
        "second": (BuildContext context) => SecondScreen(),
      },
      home: Scaffold(
        backgroundColor: AppColors().primary,
        body: Container(
          margin: EdgeInsets.only(left: 16, right: 16, top: 86),
          child: Column(
            children: [
              Padding(
                  padding: EdgeInsets.only(left: 16, right: 16),
                  child: Image.asset("assets/images/img_auth_app_logo.png")),
              Padding(
                padding: EdgeInsets.only(top: 80),
                child: TextField(
                  controller: _emailController,
                  style: TextStyle(color: Colors.white),
                  decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                          borderSide:
                          BorderSide(color: AppColors().editBorderEnable)),
                      enabledBorder: OutlineInputBorder(
                          borderSide:
                          BorderSide(color: AppColors().editBorder)),
                      disabledBorder: OutlineInputBorder(
                          borderSide:
                          BorderSide(color: AppColors().editBorder)),
                      errorText:
                      _emailValidation ? null : "Поле заполнено неверно",
                      labelText: "Логин",
                      labelStyle: TextStyle(color: AppColors().textHint)),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 32),
                child: TextField(
                  controller: _passwordController,
                  style: TextStyle(color: Colors.white),
                  obscureText: _isObscure,
                  decoration: InputDecoration(
                      suffixIcon: InkWell(
                        borderRadius: BorderRadius.circular(50),
                        splashColor: AppColors().accent,
                        onTap: () {
                          setState(() {
                            _isObscure = !_isObscure;
                          });
                        },
                        child: Icon(
                          _isObscure ? Icons.visibility : Icons.visibility_off,
                          color: AppColors().iconTint,
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderSide:
                          BorderSide(color: AppColors().editBorderEnable)),
                      enabledBorder: OutlineInputBorder(
                          borderSide:
                          BorderSide(color: AppColors().editBorder)),
                      disabledBorder: OutlineInputBorder(
                          borderSide:
                          BorderSide(color: AppColors().editBorder)),
                      errorText:
                      _passValidation ? null : "Поле заполнено неверно",
                      labelText: "Пароль",
                      labelStyle: TextStyle(color: AppColors().textHint)),
                ),
              ),
              Padding(
                  padding: EdgeInsets.only(top: 68),
                  child: SizedBox(
                    width: double.infinity,
                    height: 52,
                    child: ElevatedButton(
                      style: ButtonStyle(
                          backgroundColor:
                          MaterialStateProperty.all(AppColors().accent)),
                      child: Text(
                        "Логин",
                        style: TextStyle(fontSize: 18),
                      ),
                      onPressed: () {
                        _authUser();
                      },
                    ),
                  ))
            ],
          ),
        ),
      ),
      locale: DevicePreview.locale(context),
      // Add the locale here
      builder: DevicePreview.appBuilder, // Add the builder here
    );
  }

  void saveFCMToken(String? token) async {
    var tokenBox = await Hive.openBox('main');
    tokenBox.put('fcmToken', token);
    // Dio dio = Dio();
    // dio.interceptors.add(LogInterceptor(responseBody: false));
    // RestClient _client = RestClient(dio);
    //
    // if(token != null) {
    //   _client.sendFCMToken("FCM", token, true);
    // }
  }

  void saveAPNSToken(String? token) async {
    var tokenBox = await Hive.openBox('main');
    tokenBox.put('apnsToken', token);
  }

  void saveUserToken(String? authToken) async {
    var tokenBox = await Hive.openBox('main');
    tokenBox.put('authToken', authToken);
  }

  void _authUser() {
    String email = _emailController.text;
    String pass = _passwordController.text;

    setState(() {
      _emailValidation = true;
      _passValidation = true;
    });

    if (!RegExp(
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(email)) {
      setState(() {
        _emailValidation = false;
      });
      return;
    }

    if (pass.isEmpty) {
      setState(() {
        _passValidation = false;
      });
      return;
    }

    Dio dio = Dio();
    dio.interceptors.add(LogInterceptor(responseBody: false));
    RestClient _client = RestClient(dio);

    _client
        .userLogin(email, pass)
        .then((value)  {
      saveUserToken(value.access);
    NavigationService.instance.navigateToReplacement("second");
    })
        .catchError((Object data) {
      print(data.toString());
      final res = (data as DioError).response;
      if (res == null) {
        Fluttertoast.showToast(
            msg: "Отсутствует интернет соединение или сервер недоступен",
            toastLength: Toast.LENGTH_SHORT);
      } else {
        Fluttertoast.showToast(msg: "Проверьте правильность введённых данных");
      }
    });
  }
}

class _IosUIState extends State<IosUi> {
  @override
  Widget build(BuildContext context) {
    return CupertinoApp(
        home: CupertinoPageScaffold(
          backgroundColor: AppColors().primary,
          child: Container(
            margin: EdgeInsets.only(left: 16, right: 16, top: 86),
            child: Column(children: [
              Padding(
                  padding: EdgeInsets.only(left: 16, right: 16),
                  child: Image.asset("assets/images/img_auth_app_logo.png")),
              Padding(
                padding: EdgeInsets.only(top: 80),
              )
            ]),
          ),
        ));
  }
}
