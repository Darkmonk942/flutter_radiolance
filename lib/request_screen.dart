import 'package:all_vet_pharm/model/RequestFullModel.dart';
import 'package:all_vet_pharm/theme/colors.dart';
import 'package:all_vet_pharm/theme/styles.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hive/hive.dart';

import 'network/RestClient.dart';

class RequestScreen extends StatelessWidget {
  final int requestId;

  RequestScreen({Key? key, required this.requestId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: WillPopScope(
      child: Scaffold(
        appBar: AppBar(
            brightness: Brightness.dark,
            backgroundColor: AppColors().primary,
            title: Text("Заявка"),
            leading: IconButton(
              icon: SvgPicture.asset("assets/images/ic_ios_back.svg"),
              onPressed: () => Navigator.of(context).pop(),
            )),
        body: _RequestCard(requestId),
      ),
      onWillPop: () async {
        Navigator.pop(context);
        return false;
      },
    ));
  }
}

class _RequestCard extends StatefulWidget {
  _RequestCard(this.requestId);

  final int requestId;

  @override
  _RequestCardState createState() => new _RequestCardState(requestId);
}

class _RequestCardState extends State<_RequestCard> {
  _RequestCardState(this.requestId);

  final int requestId;
  late Future<RequestFullModel> request;

  @override
  void initState() {
    super.initState();
    request = getRequest();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<RequestFullModel>(
        future: request,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Container(
              color: AppColors().primary,
              constraints: BoxConstraints.expand(),
              child: Card(
                elevation: 4,
                margin: EdgeInsets.all(16),
                color: AppColors().secondary,
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          snapshot.data!.number,
                          style: AppStyles.textSecondary,
                        ),
                        SizedBox(height: 12),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SvgPicture.asset("assets/images/ic_request.svg"),
                            SizedBox(width: 12),
                            Flexible(
                              child: Text(
                                snapshot.data!.services[0].name,
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                style: AppStyles.textMain,
                              ),
                            )
                          ],
                        ),
                        SizedBox(height: 8),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            snapshot.data!.customer.logoUrl == null
                                ? SvgPicture.asset("assets/images/ic_request.svg")
                                : Image.network(
                                    snapshot.data!.customer.logoUrl!,
                                    height: 40,
                                    width: 40,
                                  ),
                            SizedBox(width: 12),
                            Flexible(
                              child: Text(
                                snapshot.data!.customer.name,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: AppStyles.textMain,
                              ),
                            )
                          ],
                        ),
                        SizedBox(height: 12),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                                "assets/images/ic_medical_device.svg"),
                            SizedBox(width: 12),
                            Flexible(
                              child: Text(
                                snapshot.data!.equipment,
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                style: AppStyles.textMain,
                              ),
                            )
                          ],
                        ),
                        SizedBox(height: 12),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SvgPicture.asset("assets/images/ic_cash.svg"),
                            SizedBox(width: 16),
                            Flexible(
                              child: Text(
                                snapshot.data!.getAmount(),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: AppStyles.textMain,
                              ),
                            )
                          ],
                        ),
                        Divider(
                          height: 32,
                          color: AppColors().dividerMain,
                        ),
                        Text(
                          "Пациент:",
                          style: AppStyles.textSecondary,
                        ),
                        SizedBox(height: 8),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              snapshot.data!.patient.isMale()
                                  ? "Мужчина"
                                  : "Женщина",
                              style: AppStyles.textMain,
                            ),
                            new Spacer(),
                            Text(
                              "45 лет",
                              style: AppStyles.textMain,
                            ),
                          ],
                        ),
                        Divider(
                          height: 32,
                          color: AppColors().dividerMain,
                        ),
                        Text(
                          "Клиническая задача:",
                          style: AppStyles.textSecondary,
                        ),
                        SizedBox(height: 8),
                        Text(
                          snapshot.data!.clinicalTask,
                          style: AppStyles.textMain,
                        ),
                        SizedBox(height: 12),
                        Text(
                          "Жалобы:",
                          style: AppStyles.textSecondary,
                        ),
                        SizedBox(height: 8),
                        Text(
                          snapshot.data!.complains.isEmpty ? "-" : snapshot.data!.complains,
                          style: AppStyles.textMain,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          }
          return Container(
              color: AppColors().primary, constraints: BoxConstraints.expand());
        });
  }

  Future<RequestFullModel> getRequest() async {
    final mainBox = await Hive.openBox('main');

    final String authToken = mainBox.get('authToken');

    Dio dio = Dio();
    dio.interceptors.add(LogInterceptor(responseBody: false));
    dio.options.headers = {"Authorization": "Bearer " + authToken};
    RestClient _client = RestClient(dio);

    return _client.getRequest(requestId);
  }
}
