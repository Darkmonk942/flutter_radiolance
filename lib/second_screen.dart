import 'dart:io';

import 'package:all_vet_pharm/main.dart';
import 'package:all_vet_pharm/model/RequestModel.dart';
import 'package:all_vet_pharm/request_screen.dart';
import 'package:all_vet_pharm/theme/colors.dart';
import 'package:all_vet_pharm/theme/styles.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:restart_app/restart_app.dart';

import 'navigation.dart';
import 'network/RestClient.dart';

class SecondScreen extends StatelessWidget {
  SecondScreen() {
    //sendPushToken();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
          backgroundColor: AppColors().primary,
          appBar: AppBar(
            brightness: Brightness.dark,
            backgroundColor: AppColors().primary,
            key: Key("app_bar"),
            title: Text("Завки"),
          ),
          body: _RequestList()),
    );
  }

  void sendPushToken() async {
    final mainBox = await Hive.openBox('main');

    final String authToken = mainBox.get('authToken');
    final String? fcmToken = mainBox.get('fcmToken');
    final String? apnsToken = mainBox.get('apnsToken');

    Dio dio = Dio();
    dio.interceptors.add(LogInterceptor(responseBody: false));
    dio.options.headers = {"Authorization": "Bearer " + authToken};
    RestClient _client = RestClient(dio);

    if (fcmToken != null && Platform.isAndroid) {
      _client.sendFCMToken("FCM", fcmToken, true);
    }

    if (apnsToken != null && Platform.isIOS) {
      _client.sendAPNSToken(apnsToken, true);
    }
  }
}

class _RequestList extends StatefulWidget {
  @override
  RequestListState createState() => new RequestListState();
}

class RequestListState extends State<_RequestList> {
  List<RequestModel> requests = <RequestModel>[];

  @override
  void initState() {
    getRequests();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
      physics: BouncingScrollPhysics(),
      itemCount: requests.length,
      itemBuilder: (BuildContext context, int index) {
        return InkWell(
          onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => RequestScreen(
                        requestId: requests[index].id,
                      ))),
          child: Container(
            child: new Card(
              elevation: 4,
              margin: EdgeInsets.only(left: 12, right: 12, top: 8),
              color: AppColors().secondary,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      requests[index].number,
                      style: AppStyles.textSecondary,
                    ),
                    SizedBox(height: 12),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SvgPicture.asset("assets/images/ic_request.svg"),
                        SizedBox(width: 12),
                        Flexible(
                          child: Text(
                            requests[index].zones[0].name,
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: AppStyles.textMain,
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 8),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        requests[index].customer.logoUrl == null
                            ? SvgPicture.asset("assets/images/ic_request.svg")
                            : Image.network(
                                requests[index].customer.logoUrl!,
                                height: 40,
                                width: 40,
                              ),
                        SizedBox(width: 12),
                        Flexible(
                          child: Text(
                            requests[index].customer.name,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: AppStyles.textMain,
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  void getRequests() async {
    final mainBox = await Hive.openBox('main');

    final String authToken = mainBox.get('authToken');

    Dio dio = Dio();
    dio.interceptors.add(LogInterceptor(responseBody: false));
    dio.interceptors
        .add(InterceptorsWrapper(onError: (DioError e, handler) {
      print("Set Data");
      Hive.deleteFromDisk();
      Restart.restartApp();
    }));
    dio.options.headers = {"Authorization": "Bearer " + authToken};
    RestClient _client = RestClient(dio);

    _client.getRequests().then((value) => setData(value.requests!));
  }

  void setData(List<RequestModel> requests) {
    this.setState(() {
      this.requests = requests;
    });
  }
}
